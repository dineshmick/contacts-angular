import { NgModule } from '@angular/core';

import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';


const modules = [
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatButtonModule,
    MatSelectModule,
    MatSidenavModule,
    MatToolbarModule
]

@NgModule({
    imports: [
        modules
    ],
    exports: [
        modules
    ]
})

export class AppMaterialModule { }
