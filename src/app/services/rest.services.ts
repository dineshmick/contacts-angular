import { throwError, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const apiUrl = environment.api;

@Injectable({
  providedIn: 'root',
})
export class RestService {

  constructor(
    protected httpClient: HttpClient
  ) { }


  public handleError(error: HttpErrorResponse, url: string) {
    if (error.error instanceof ErrorEvent) {
      // Client-side or network error
      console.error(url, error.error.message);
    } else {
      // Backend returned an unsuccessful response code
      console.error(url, `code: ${error.status}, body: ${error.error}`);
    }
    // Return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }


  getAllContacts(): Observable<any> {

    return this.httpClient.get<any[]>(`${apiUrl}/contacts`)
      .pipe(
      catchError((err) => this.handleError(err, `${apiUrl}/contacts`))
      );
  }

  
  fetchRecords(value, lname): Observable<any> {

    return this.httpClient.get<any[]>(`${apiUrl}/contacts?first_name`+"_like="+value+`&last_name`+"_like="+lname)
      .pipe(
      catchError((err) => this.handleError(err, `${apiUrl}/contacts`))
      );
  }

  updateRecords(record): Observable<any> {

    return this.httpClient.put<any[]>(`${apiUrl}/contacts/`+`${record.id}`, record)
      .pipe(
      catchError((err) => this.handleError(err, `${apiUrl}/contacts`))
      );
  }

}
