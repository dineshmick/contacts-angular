import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { RestService } from 'src/app/services/rest.services';

@Component({
  selector: 'app-employee-contacts',
  templateUrl: './employee-contacts.component.html',
  styleUrls: ['./employee-contacts.component.scss']
})
export class EmployeeContactsComponent implements OnInit {

  data: any;
  filteredData: any;
  isShowDrawer: boolean = false;
  drawerData: any;
  over = "over";
  name: string;
  select: string;
  searchOptions = [
    { value: 'first_name', viewValue: 'First Name' },
    { value: 'last_name', viewValue: 'Last Name' },
    { value: 'email', viewValue: 'Email' }
  ];
  constructor(private restService: RestService) { }

  ngOnInit(): void {
    this.restService.getAllContacts().subscribe(res => {
      console.log(res);
      this.data = res;
    })
  }

  onSubmit(form: NgForm) {
    this.restService.fetchRecords(form.value.name, form.value.lname).subscribe(res => {
      console.log("filtered", res);
      this.filteredData = res;
    })
  }

  showDrawer(drawer, data) {
     //drawer.toggle();
     this.isShowDrawer = true;
     this.drawerData = data;
  }

  editRecords(name) {
    console.log(this.drawerData);
    this.drawerData.company_name = name;
    this.isShowDrawer = false;
    this.restService.updateRecords(this.drawerData).subscribe(res=>{
      console.log(res);

    })
  }

  backToScreen() {
    this.isShowDrawer = false;
  }
  getRandomColor() {
    var rgb = [];
    for (let i = 0; i < 3; i++) {
      rgb.push(Math.floor(Math.random() * 255));
    }
    return 'rgb(' + rgb.join(',') + ')';
    // var colors = ['#808080', '#ADD8E6'];
    // var random_color = colors[Math.floor(Math.random() * colors.length)];
    //     return random_color;
  }
}
